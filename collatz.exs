defmodule Integer.Guards do
  defguard is_even(value) when rem(value, 2) == 0
end

defmodule Collatz do
  import Integer.Guards

  def converge(n) when n > 0, do: step(n, 0)

  defp step(1, step_count), do: step_count
  defp step(n, step_count) when is_even(n),
       do: n
           |> div(2)
           |> step(step_count + 1)

  defp step(n, step_count),
       do: n
           |> Kernel.*(3)
           |> Kernel.+(1)
           |> step(step_count + 1)
end

IO.puts(Collatz.converge(42))
