defmodule Integer.Guards do
  defguard is_even(value) when rem(value, 2) == 0
end

defmodule Collatz do
  import Integer.Guards

  def converge(n) when n > 0, do: step(n, [])

  defp step(1, folge), do: folge ++ [1]

  defp step(n, folge) when is_even(n) do
    n
    |> div(2)
    |> step(folge ++ [n])
  end

  defp step(n, folge) do
    n
    |> Kernel.*(3)
    |> Kernel.+(1)
    |> step(folge ++ [n])
  end
end

Enum.each(Collatz.converge(420), &IO.puts/1)
