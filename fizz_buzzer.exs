defmodule FizzBuzzer do
  def fizzbuzz (n) do
    teilbarDurch3 = rem(n, 3) == 0
    teilbarDurch5 = rem(n, 5) == 0

    cond do
      teilbarDurch3 && teilbarDurch5 -> "FizzBuzz"
      teilbarDurch3 -> "Fizz"
      teilbarDurch5 -> "Buzz"
      true -> n
    end
  end
end

1..100
|> Enum.map(&FizzBuzzer.fizzbuzz/1)
|> Enum.each(&IO.puts/1)
